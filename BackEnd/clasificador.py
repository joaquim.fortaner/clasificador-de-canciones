import pandas as pd
import operator
from nltk.corpus import stopwords

class Clasificador:
    def __init__(self):
        self.caracter_remove = ['(', ')', '¡', '!', '¿', '?', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
                                '.', ',', ':', '\\', '\x80', '\x99', '\x94', '\x98']
        self.vocabulario = []
        self.array_happy = []
        self.array_nostalgia = []
        self.array_rage = []
        self.array_pal_happy = []
        self.array_pal_nostagia = []
        self.array_pal_rage = []

        self.pro_happy = 1
        self.pro_nostalgia = 1
        self.pro_rage = 1

    def leer(self):
        self.df = pd.read_csv('..\\lyrics.csv')
        self.lyrics = self.df['lyrics']
        self.category = self.df['category']


    def limpieza(self, palabra):
        palabra = palabra.replace('â', "'")
        palabra = palabra.lower()
        for c in self.caracter_remove:
            palabra = palabra.replace(c, '')

        stop_words = set(stopwords.words("english"))
        if palabra not in stop_words:
            if palabra != "hey":
                if palabra != "ya":
                    if palabra != "yeah":
                        return palabra

        return -1

    def generar_vocabulario(self, palabra):
        if palabra not in self.vocabulario:
            self.vocabulario.append(palabra)

    def organizar(self):
        for i in range(len(self.category)):
            l = str(self.lyrics[i]).split()
            for palabra in l:
                palabra = self.limpieza(palabra)
                if palabra != -1:
                    self.generar_vocabulario(palabra)
                    if self.category[i] == '0':
                        self.array_happy.append(palabra)
                    elif self.category[i] == '1':
                        self.array_nostalgia.append(palabra)
                    elif self.category[i] == '2':
                        self.array_rage.append(palabra)

    def calculo_palabras(self, array_sent, newLyric):
        calculo_array = []

        for palabra in newLyric:
            calculo_array.append((array_sent.count(palabra) + 1) / (len(array_sent) + len(self.vocabulario)))

        return calculo_array

    def calculo_sentimientos(self):
        self.pro_happy = 1
        self.pro_nostalgia = 1
        self.pro_rage = 1

        for i in range(len(self.array_pal_nostagia)):
            self.pro_happy *= (self.array_pal_happy[i] * 1000)
            self.pro_nostalgia *= (self.array_pal_nostagia[i] * 1000)
            self.pro_rage *= (self.array_pal_rage[i] * 1000)

    def calculo_pro_sent(self):
        cont_happy = 0
        cont_nost = 0
        cont_rage = 0

        for cat in self.category:
            if cat == '0':
                cont_happy += 1
            elif cat == '1':
                cont_nost += 1
            elif cat == '2':
                cont_rage += 1

        happy = (cont_happy / len(self.category)) * self.pro_happy
        nostalgia = (cont_nost / len(self.category)) * self.pro_nostalgia
        rage = (cont_rage / len(self.category)) * self.pro_rage

        sentimientos = {'happy': happy, 'nostalgia': nostalgia, 'rage': rage}

        sort_sentimientos = sorted(sentimientos.items(), key=operator.itemgetter(1), reverse=True)

        return sort_sentimientos

    def entrenar(self):
        self.leer()
        self.organizar()

        vocabulario = open('vocabulario.txt', 'w', encoding="utf-8")
        for palabra in self.vocabulario:
            vocabulario.write(palabra + " ")
        vocabulario.close()

        happy = open('happy.txt', 'w', encoding="utf-8")
        for palabra in self.array_happy:
            happy.write(palabra + " ")
        happy.close()

        nostalgia = open('nostalgia.txt', 'w', encoding="utf-8")
        for palabra in self.array_nostalgia:
            nostalgia.write(palabra + " ",)
        nostalgia.close()

        rage = open('rage.txt', 'w', encoding="utf-8")
        for palabra in self.array_rage:
            rage.write(palabra + " ")
        rage.close()

        category = open('category.txt', 'w', encoding="utf-8")
        for palabra in self.category:
            category.write(str(palabra) + " ")
        category.close()

    def calcular(self, newLyric):
        f = open('vocabulario.txt', 'r', encoding="utf-8")
        mensaje = f.read()
        self.vocabulario = mensaje.split()
        f.close()

        f = open('happy.txt', 'r', encoding="utf-8")
        mensaje = f.read()
        self.array_happy = mensaje.split()
        f.close()

        f = open('nostalgia.txt', 'r', encoding="utf-8")
        mensaje = f.read()
        self.array_nostalgia = mensaje.split()
        f.close()

        f = open('rage.txt', 'r', encoding="utf-8")
        mensaje = f.read()
        self.array_rage = mensaje.split()
        f.close()

        f = open('category.txt', 'r', encoding="utf-8")
        mensaje = f.read()
        self.category = mensaje.split()
        f.close()

        new = []

        for palabra in newLyric:
            palabra = self.limpieza(palabra)
            if palabra != -1:
                new.append(palabra)

        self.array_pal_happy = self.calculo_palabras(self.array_happy, new)
        self.array_pal_nostagia = self.calculo_palabras(self.array_nostalgia, new)
        self.array_pal_rage = self.calculo_palabras(self.array_rage, new)

        self.calculo_sentimientos()
        sentimientos = self.calculo_pro_sent()
        return sentimientos
