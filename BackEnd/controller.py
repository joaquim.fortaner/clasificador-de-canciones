import os
from flask import Flask, flash, request, redirect, url_for, jsonify
from werkzeug.utils import secure_filename
from clasificador import Clasificador


app = Flask(__name__)
sentimientos = []
C = Clasificador()



def leerCancion(name):
    cancion = name.split()
    return cancion

@app.route('/entrenar', methods=['GET','POST'])
def entrenarModelo():
        C.entrenar()
        response = jsonify({'status': 201})
        return response

@app.route('/getFile', methods=['GET','POST'])
def publish():
    if request.method == 'GET':
        message = request.args['text']
        if message == " ": 
            response = jsonify({'status': 404,'error': 'not found','data': 'No text'})
            response.status_code = 404
            return response

        else:
            name = request.args['text']
            letra = leerCancion(name)
            sentimientos = C.calcular(letra)
            sentimiento1 = sentimientos[0][0:2]
            sentimiento2 = sentimientos[1][0:2]
            response = jsonify({'status': 201,'error': '','emocion1': sentimiento1,'emocion2': sentimiento2})
            response.status_code = 201
            return response  

            
