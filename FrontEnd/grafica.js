google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ["Emocion", "Porcentaje"],
        ["Felicidad", 321],
        ["Nostalgia", 654]
    ]);

    var options = {
        title: "Emotions",
        pieHole: 0.4,
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
}